import React from 'react';

class SignUpForm extends React.Component{
  render(){
    return(
      <form>
        <h1> SIGN UP </h1>

        <div className="form-group">
          <label className="control-label"> First Name </label>
          <input
            type="text"
            name="firstName"
            className="form-control"
          />
        </div>

        <div className="form-group">
          <label className="control-label"> Last Name </label>
          <input
            type="text"
            name="lastName"
            className="form-control"
          />
        </div>

        <div className="form-group">
          <label className="control-label"> Email </label>
          <input
            type="text"
            name="email"
            className="form-control"
          />
        </div>

        <div className="form-group">
          <label className="control-label"> Password </label>
          <input
            type="password"
            name="password"
            className="form-control"
          />
        </div>

        <div className="form-group">
          <button className="btn btn-primary btn-lg">
            Sign up
          </button>
        </div>

      </form>
    );
  }
}

export default SignUpForm;
