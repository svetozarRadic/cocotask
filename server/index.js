const schedule = require('node-schedule')
const axios = require('axios')
const email = require('./sendEmail')
const config = require('./config/config')

const url = `http://api.hnb.hr/tecajn/v1?valuta=${config.requirements.currency}`
let message = ''
let rateCurrency


let oldRate = 0

const getCurrentValue = (hnbData) => {
  const rateInformation = (hnbData[0] || {})
  const currentValueOfCurrency = rateInformation['Srednji za devize']

  rateCurrency = rateInformation.Valuta
  message = `Rate for ${rateCurrency} is ${currentValueOfCurrency} kn.`
  console.log(message)

  return currentValueOfCurrency
}

const isFivePercentChange = currentValueOfCurrency => (
  ((oldRate + config.requirements.threshold * oldRate) <= currentValueOfCurrency)
  || ((oldRate - config.requirements.threshold * oldRate) >= currentValueOfCurrency))

schedule.scheduleJob('0 0 * * *', async () => {
  try {
    const response = await axios.get(url)
    const currentValueOfCurrency = getCurrentValue(response.data)

    if (isFivePercentChange(currentValueOfCurrency)) {
      email.sendEmail('"Admin" <sveto1997@gmail.com>', 'sveto1997@gmail.com', 'HELLO', '<p>Currency is changed  </p>')
    }
    oldRate = currentValueOfCurrency
  } catch (err) {
    console.log(err)
  }
})

// const test = (async () => {
//   try {
//     const response = await axios.get(url)
//     const currentValueOfCurrency = getCurrentValue(response.data)
//     console.log(response.data)
//   } catch (err) { console.log(err) }
// })
//
// test()
// http.createServer(function(req,res){
//   res.end(message);
// }).listen(3000);
module.exports = { getCurrentValue }
