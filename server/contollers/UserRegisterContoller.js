
const jwt = require('jsonwebtoken')
const User = require('../models/User')
const config = require('../config/config')
const emailVerification = require('../sendEmail')

async function sendEmailVerification(user) {
  const token = jwt.sign(user,
    config.authentication.jwtSecret,
    (err, emailToken) => {
      if (err) {
        console.log(err)
      }
      const url = `http://localhost:5000/users/confirmation/${emailToken}`
      emailVerification.sendEmail('"Admin" <sveto1997@gmail.com>', user.email, 'Confirm Email', `Please click this email to confirm your email: <a href="${url}">${url}</a>`)
    })
}

module.exports = {
  register(req, res) {
    const { body } = req
    const {
      firstName,
      lastName,
      password,
      date,
    } = body
    let {
      email,
    } = body

    email = email.toLowerCase()

    User.find({
      email: req.body.email,
    }, (err, previousUser) => {
      if (err) {
        return res.send({ success: false, message: 'Error:Server error' })
      }
      if (previousUser.length > 0) {
        return res.send({ success: false, message: 'Account already exists' })
      }
      const newUser = new User(body)
      newUser.password = newUser.generateHash(password)
      newUser.save((err, user) => {
        if (err) {
          return res.send({ success: false, message: 'Error:Server error' })
        }
        if (!user) {
          return res.send({ success: false, message: 'User can not be saved ' })
        }
        const userJson = user.toJSON()
        sendEmailVerification(userJson)
        return res.send({
          userJson,
          success: true,
          message: 'Signed up!',
        })
      })
    })
  },
}
