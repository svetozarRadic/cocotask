
const jwt = require('jsonwebtoken')
const UserSession = require('../models/UserSession')
const User = require('../models/User')
const config = require('../config/config')


function createToken(user) {
  const ONE_DAY = 60 * 60 * 24
  return jwt.sign(user, config.authentication.jwtSecret, {
    expiresIn: ONE_DAY,
  })
}

function createRefreshToken(user) {
  const ONE_MONTH = 60 * 60 * 24 * 30
  return jwt.sign(user, config.authentication.jwtSecret, {
    expiresIn: ONE_MONTH,
  })
}
async function validate(body, user, res) {
  if (!body.email) {
    return res.status(403).send({
      error: 'Email field can not be empty!',
    })
  }
  if (!user) {
    return res.status(403).send({
      error: 'User does not exists! Please sign up!',
    })
  }
  if (!user.confirmedEmail) {
    return res.status(403).send({
      error: 'Please verify your email address first!',
    })
  }
  const isPasswordValid = await user.validPassword(body.password)
  if (!isPasswordValid) {
    return res.status(403).send({
      error: 'Invalid password!',
    })
  }
}

module.exports = {
  async login(req, res) {
    try {
      const { email, password } = req.body
      const user = await User.findOne({
        email: req.body.email,
      })
      validate(req.body, user, res)

      const userSession = new UserSession()
      const userJson = user.toJSON()
      userSession.token = createToken(userJson)
      userSession.refreshToken = createRefreshToken(userJson)
      userSession.save((err) => {
        if (err) {
          console.log(err)
          return res.send({
            success: false,
            message: 'Error: server error',
          })
        }
        return res.status(200).send({
          success: true,
          message: 'Valid log in',
        })
      })
    } catch (error) {
      return res.status(400).send({
        error: 'Server error',
      })
    }
  },
}
