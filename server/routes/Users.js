const express = require('express')

const users = express.Router()
const cors = require('cors')
const UserRegisterPolicy = require('../policies/UserRegisterPolicy')
const UserRegisterContoller = require('../contollers/UserRegisterContoller')
const UserLoginController = require('../contollers/UserLoginController')
const Authentication = require('../policies/Authentication')

users.use(cors())

users.post('/register', UserRegisterPolicy.register, UserRegisterContoller.register)
users.post('/login', UserLoginController.login)
users.get('/confirmation/:token', Authentication.isAuthenticated)

module.exports = users
