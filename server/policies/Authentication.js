const jwt = require('jsonwebtoken')
const User = require('../models/User')
const config = require('../config/config')

module.exports = {
  isAuthenticated(req, res) {
    jwt.verify(req.params.token, config.authentication.jwtSecret, (err) => {
      // err
      if (err) {
        return res.send({ message: err })
      }
    })

    const user = jwt.verify(req.params.token, config.authentication.jwtSecret)

    User.findOneAndUpdate(
      { email: user.email },
      { confirmedEmail: true },
      (err) => {
        if (err) {
          console.log('Error while updating')
        }
        return res.status(200).send({
          success: true,
          message: 'You have successfully confirmed your email address! Now you can log in',
        })
      },
    )
  },
}
