const Joi = require('joi')

module.exports = {
  register(req, res, next) {
    const schema = {
      firstName: Joi.string().alphanum().min(2).max(32)
        .required(),
      lastName: Joi.string().alphanum().min(2).max(32)
        .required(),
      email: Joi.string().email(),
      password: Joi.string().regex(
        new RegExp('^[a-zA-z0-9]{8,32}$'),
      ),
    }
    const { error } = Joi.validate(req.body, schema)
    if (error) {
      console.log('error', error)
      switch (error.details[0].context.key) {
        case 'firstName':
          res.status(400).send({
            error: `Provided First name must obide the following rules:
            <br>
            1. It can not be blank
            <br>
            2. It can contain only alphanumeric characters
            <br>
            3. It must be at least 2 characters in length and less than 32 characters in length
            `,
          })
          break
        case 'lastName':
          res.status(400).send({
            error: `Provided Last name must obide the following rules:
              <br>
              1. It can not be blank
              <br>
              2. It can contain only alphanumeric characters
              <br>
              3. It must be at least 2 characters in length and less than 32 characters in length
              `,
          })
          break
        case 'email':
          res.status(400).send({
            error: 'You must enter a valid email address.',
          })
          break
        case 'password':
          res.status(400).send({
            error: `Provided password must obide the following rules:
            <br>
            1. It can contain only capital and non-capital letters, and numbers
            <br>
            2. It must be at least 8 characters in length and less than 32 characters in length
            `,
          })
          break
        default:
          res.status(400).send({
            error: 'Invalid registration data.',
          })
      }
    } else {
      next()
    }
  },
}
