const express = require('express')
const nodemailer = require('nodemailer')
const dotenv = require('dotenv').load()


const app = express()

// Check server with sending a message
app.get('/', (req, res) => {
  res.send('Hello')
})

// create reusable transporter object using the default SMTP transport
const transporter = nodemailer.createTransport({
  service: 'gmail',
  // true for 465, false for other ports
  auth: {
    user: process.env.EMAIL_USERNAME, // replace with your own email
    pass: process.env.EMAIL_PASS, // replace with your own pass
    // generated ethereal password
  },
  tls: {
    rejectUnauthorized: false,
  },
})

module.exports = {
  sendEmail(from, to, subject, html) {
    transporter.sendMail({
      from, to, subject, html,
    }, (error, info) => {
      if (error) {
        console.log(error)
      }
      console.log('Message sent: %s', info.accepted)
      // Preview only available when sending through an Ethereal account
      console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info))

      // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
      // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
    })
  },

}


// app.listen(3000,()=>console.log("server started.."));
