const data = require('../index')

test('expected value of EUR to be 7,397789,', () => {
  const dataAPI = [{
    'Broj tečajnice': '14', 'Datum primjene': '21.01.2019', Država: 'EMU', 'Šifra valute': '978', Valuta: 'EUR', Jedinica: 1, 'Kupovni za devize': '7,402279', 'Srednji za devize': '7,424553', 'Prodajni za devize': '7,446827',
  }]
  expect(data.getCurrentValue(dataAPI)).toBe('7,424553')
})
