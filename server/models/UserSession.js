const mongoose = require('mongoose')

const UserSessionSchema = new mongoose.Schema({
  token: {
    type: String,
    default: '',
  },
  refreshToken: {
    type: String,
    default: '',
  },
  timestamp: {
    type: Date,
    default: Date.now(),
  },
  isDeleted: {
    type: Boolean,
    default: false,
  },
})
module.exports = mongoose.model('UserSession', UserSessionSchema)
